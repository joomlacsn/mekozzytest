<?php
/**
 * @package    api
 * @subpackage C:
 * @author     Hau Pham {@link jooext.com}
 * @author     Created on 02-Oct-2017
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

?>
<style>
    .sidebar {
        position: relative !important;
        float: left;
    }

    .nav-tabs .nav-item .nav-link, .nav-tabs .nav-item .nav-link:focus, .nav-tabs .nav-item .nav-link {
        color: #1c1c1c !important;
    }

    .nav-tabs .nav-item .nav-link:hover {
        color: #0a0a0a !important;
    }

</style>

<div class="wrapper " style="z-index: 5" ng-controller="MainController">

    <toaster-container
            toaster-options="{'position-class': 'toast-bottom-right', 'progress-bar': true, 'time-out':2000}"></toaster-container>

    <div class="sidebar" data-color="green" data-background-color="white">

        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="#0">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link"
                       href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.orders'); ?>">
                        <i class="fa fa-cart-plus"></i>
                        <p>Đơn hàng</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link"
                       href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.customers'); ?>">
                        <i class="fa fa-users"></i>
                        <p>Khách hàng</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link"
                       href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.khohang'); ?>">
                        <i class="fa fa-database"></i>
                        <p>Kho hàng</p>
                    </a>
                </li>


                <li class="nav-item ">
                    <a class="nav-link"
                       href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.products'); ?>">
                        <i class="fa fa-product-hunt"></i>
                        <p>Sản phẩm</p>
                    </a>
                </li>

                <li class="nav-item active ">
                    <a class="nav-link"
                       href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.config'); ?>">
                        <i class="fa fa-cogs"></i>
                        <p>Cấu hình</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="#0">
                        <i class="fa fa-question-circle"></i>
                        <p>Hướng dẫn sử dụng</p>
                    </a>
                </li>
            </ul>
            <div class="container-fluid" style="margin-top: 20px">

                <div class="copyright float-center">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    , developed by <a href="https://www.medcomis.com" target="_blank">Hau Pham</a>.
                </div>
            </div>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="javascript:">Cấu hình hệ thống</a>
                </div>
                <div class="collapse navbar-collapse justify-content-end">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:">
                                <i class="material-icons">notifications</i> Notifications
                            </a>
                        </li>
                        <!-- your navbar here -->

                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content" style="margin-top: 2em">
            <div class="container-fluid">

                <uib-tabset active="active">
                    <uib-tab index="0">
                        <uib-tab-heading>
                            <i class="fa fa-info-circle"></i> Thông tin gian hàng
                        </uib-tab-heading>
                        <uib-tab-content>
                            <div class="row-fluid">
                                <br/>
                                <a href="/control-panel/account-maintenance" target="_blank">Đến trang cấu hình</a>
                            </div>
                        </uib-tab-content>

                    </uib-tab>
                    <uib-tab index="1">
                        <uib-tab-heading>
                            <i class="fas fa-glasses"></i> Giao diện
                        </uib-tab-heading><br/>
                        Cấu hình giao diện
                    </uib-tab>
                </uib-tabset>

            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <!-- your content here -->
            </div>
        </footer>
    </div>
</div>

<script type="text/javascript">

    myApp = angular.module("myApp", ['toaster', 'ui.bootstrap']);

    myApp.controller('MainController', ['$scope', '$http', 'toaster', '$interval', function ($scope, $http, toaster, $interval) {


        angular.element(document).ready(function() {


            trumbowygObj = jQuery('textarea#formControlFullDescription')
                .trumbowyg({
                    lang:"vi",
                    autogrow: true,
                    imageWidthModalEdit: true,
                    btns: [
                        ['viewHTML'],
                        ['formatting'],
                        ['strong', 'em', 'del'],
                        ['superscript', 'subscript'],
                        ['link'],
                        ['insertImage'], // Our fresh created dropdown
                        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                        ['unorderedList', 'orderedList'],
                        ['horizontalRule'],
                        ['removeformat'],
                        ['fullscreen']
                    ]
                });

            function formatLink(cellValue, options, rowObject) {
                var urlHtml = "<a href='" + cellValue + "' target='_blank'>##</a>";
                return urlHtml;
            }

        });


    }]);

    myApp.directive('vendorForm', function(){
        return {
            restrict: 'AEC',
            templateUrl: '<?php echo JUri::root(true)?>/media/com_congtacvien/templates/vendor_form.html'
        }
    });


</script>