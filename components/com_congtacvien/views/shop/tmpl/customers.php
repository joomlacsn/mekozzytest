<?php
/**
 * @package    api
 * @subpackage C:
 * @author     Hau Pham {@link jooext.com}
 * @author     Created on 02-Oct-2017
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

?>
<style>
    .sidebar {
        position: relative !important;
        float: left;
    }

    .product-small-item {
        margin: 5px 0; height: 200px;
    }

    .product-small-item_content {
        height: 100%;
        border: 1px solid #73AD21;
        border-radius: 10px;
    }

    .product-selected {
        border: 2px solid red !important;
    }

    .product-small-item_content:hover {
        border: 2px solid red;
    }

    .product-small-item_content_header {
        background-color: #3c3f41AA;
        color: white;
        font-size: 0.9em;
    }
    .product-small-item_content h2 {
        padding: 5px;
        color: white;
        font-size: 1.1em;
    }

    .product-small-item_content h2 a {
        color: white;
    }
    .product-small-item_content h2 a:hover {
        color: yellow;
    }

    .product-small-item_content > div {
        padding: 5px;
    }
    .product-small-item_content_footer {
        position: absolute;
        bottom: 0;
        text-align: center;
        padding: 0;
    }
    .product-small-item_content_footer button {
        padding: 0.3em !important;
    }
    .product-small-item_content_popup {
        width: 300px;
    }
    .product-small-item_content_price {
        color: white;
        font-weight: bold;
    }
</style>

<div class="wrapper " style="z-index: 5" ng-controller="CustomerController">

    <toaster-container toaster-options="{'position-class': 'toast-bottom-right', 'progress-bar': true, 'time-out':2000}"></toaster-container>

    <div class="sidebar" data-color="green" data-background-color="white">

        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="#0">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.orders');?>" >
                        <i class="fa fa-cart-plus"></i>
                        <p>Đơn hàng</p>
                    </a>
                </li>
                <li class="nav-item active ">
                    <a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.customers');?>" >
                        <i class="fa fa-users"></i>
                        <p>Khách hàng</p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.khohang');?>" >
                        <i class="fa fa-database"></i>
                        <p>Kho hàng</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.products');?>" >
                        <i class="fa fa-product-hunt"></i>
                        <p>Sản phẩm</p>
                    </a>
                </li>


                <li class="nav-item ">
                    <a class="nav-link" href="<?php echo JRoute::_('index.php?option=com_congtacvien&task=shop.config');?>" >
                        <i class="fa fa-cogs"></i>
                        <p>Cấu hình</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="#0">
                        <i class="fa fa-question-circle"></i>
                        <p>Hướng dẫn sử dụng</p>
                    </a>
                </li>
            </ul>
            <div class="container-fluid" style="margin-top: 20px">

                <div class="copyright float-center">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, developed by <a href="https://www.medcomis.com" target="_blank">Hau Pham</a>.
                </div>
            </div>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="javascript:;">Danh sách Khách hàng</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:;">
                                <i class="material-icons">notifications</i> Notifications
                            </a>
                        </li>
                        <!-- your navbar here -->
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="row-fluid" id="category-container">

                </div>
                <div class="row-fluid" id="orders-container">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="col-1">Mã KH</th>
                            <th class="col-3">Tên khách hàng</th>
                            <th class="col-4">Địa chỉ</th>
                            <th class="col-2">Điện thoại</th>
                            <th class="col-2">Ngày tham gia</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="customer in customers" ng-click="getCustomerProducts(customer.virtuemart_user_id)">
                            <td class="col-1">{{customer.virtuemart_user_id}}</td>
                            <td class="col-3">{{customer.customer}}</td>
                            <td class="col-4">{{customer.address}}</td>
                            <td class="col-2">{{customer.phone}}</td>
                            <td class="col-2">{{customer.created_on}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row-fluid" id="detail-container">
                    <h2>Danh sách sản phẩm</h2>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="col-1">#</th>
                                <th class="col-1">#Order</th>
                                <th class="col-2">Ngày mua</th>
                                <th class="col-1">SKU</th>
                                <th class="col-2">Tên sản phẩm</th>
                                <th class="col-2">Giá</th>
                                <th class="col-1">SL</th>
                                <th class="col-2">Image</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="product in products">
                                <td class="col-1">{{product.virtuemart_order_id}}</td>
                                <td class="col-1">{{product.order_number}}</td>
                                <td class="col-2">{{product.created_on}}</td>
                                <td class="col-1">{{product.product.product_sku}}</td>
                                <td class="col-2">{{product.product.product_name}}</td>
                                <td class="col-2 text-right">{{product.product.prices.product_price | number}}</td>
                                <td class="col-1 text-center">{{product.product.amount | number}}</td>
                                <td class="col-2"><a href="{{product.product.link}}" target="_blank">
                                        <img class="img-thumbnail" src="{{product.product.imageUrl}}" /></a></td>
                            </tr>
                            </tbody>
                        </table>
                        <pre>
                            {{product | json}}

                        </pre>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <!-- your content here -->
            </div>
        </footer>
    </div>
</div>

<script type="text/javascript">

    myApp = angular.module("myApp", ['toaster', 'ui.bootstrap']);

    myApp.controller('CustomerController', ['$scope', '$http', 'toaster', '$interval', function($scope, $http, toaster, $interval){

        $scope.customers = [];
        $scope.products = [];
        $scope.pager = {
            totalItems: 0,
            currentPage: 1,
            itemsperpage: 20
        };

        $scope.getOrders = function($pager, catid) {
            toaster.pop('info', 'Đang lấy dữ liệu...');
            let url = 'index.php?option=com_congtacvien&task=shop.getcustomers';
            $http.post(url, {'pager':$scope.pager, catid})
                .then(function(response){
                    if (response.status == 200) {
                        if (response.data.success) {
                            $scope.pager.totalItems = response.data.total;
                            $scope.pager.itemsperpage = response.data.limit;
                            $scope.pager.currentPage = response.data.limitstart;
                            $scope.customers = response.data.data;
                        } else {
                            toaster.pop("error", response.data.message, "", 0);
                        }
                    } else {
                        toaster.pop("error", response.statusText, "", "");
                    }
                    toaster.clear();
                });
        };

        $scope.getOrders($scope.pager, 0);

        $scope.pageChanged = function() {
            $scope.getOrders($scope.pager, 0);
        };

        $scope.getCustomerProducts = function (customer_id) {
            toaster.pop('info', 'Đang lấy dữ liệu...');
            let url = 'index.php?option=com_congtacvien&task=shop.getcustomerproducts&customer_id='+customer_id;
            $http.get(url)
                .then(function(response){
                    if (response.status == 200) {
                        if (response.data.success) {
                            $scope.products = response.data.data;
                        } else {
                            toaster.pop("error", response.data.message, "", 0);
                        }
                    } else {
                        toaster.pop("error", response.statusText, "", "");
                    }
                    toaster.clear();
                });
        }

    }]);

    myApp.directive('vendorOrder', function(){
        return {
            restrict: 'AEC',
            templateUrl: '<?php echo JUri::root(true)?>/media/com_congtacvien/templates/vendor_order.html'
        }
    });


</script>